#!/usr/bin/python3

"""
Display the per-commit size of the current git branch.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as md

import argparse
import os
import re
import datetime

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"],
    "mathtext.fontset": 'stixsans',
    }
    )


def main(args=None):
    parser = argparse.ArgumentParser(description='Plot the progress based on your git repo')
    parser.add_argument('--included_files', type=str, nargs='*', help='Filter files that should be excluded from stats',)
    parser.add_argument('--excluded_files', type=str, nargs='*', help='Filter files that should be excluded from stats',)
    parser.add_argument('--deadline_date', type=str, nargs=1, help='Deadline of project',)
    parser.add_argument('--diagram_title', type=str, nargs=1, help='Title of the diagram',)
    args = parser.parse_args()
    
    included_files = args.included_files
    excluded_files = args.excluded_files
    deadline_date =  args.deadline_date
    diagram_title = args.diagram_title[0]
    sys_call = f"git log --shortstat --reverse " \
               + r'--pretty=format:"%h;%ai;%s" '
    if included_files is not None:
        sys_call += r'-- '
        for incl_file in included_files:
            sys_call += f'{incl_file} '
    
    if excluded_files is not None:
        sys_call += r'-- ":(exclude)'
        sys_call += f'./{excluded_files[0]}"'
    out = os.popen(sys_call).read()
    total_files, total_insertions, total_deletions = 0, 0, 0
    totalprogression = []
    changed_lines_progression = []
    dates = []
    for line in out.split('\n'):
        if "Merge" in line:
            continue
        if len(line) == 0:
            continue
        if line[0] != ' ':
        #    pass
         #This is a description line
            hash = line.split(" ", 1)
            date = re.findall('\d+-\d+-\d+ \d+:\d+:\d+', line)
            try:
                date = date[0]
            except IndexError:
                print("Index Error occured")
                continue
            date = datetime.datetime.strptime(date, r'%Y-%m-%d %H:%M:%S')
            dates.append(date)
        
        else:
        # This is a stat line
            data = re.findall(
              '(\d+) files changed, (\d+) insertions\(\+\), (\d+) deletions\(-\)', 
              line)
            files = re.findall('(\d+) files changed', line)
            insertions = re.findall('(\d+) insertions\(\+\)', line)
            deletions = re.findall('(\d+) deletions\(-\)', line)
            files = int(files[0]) if len(files)>0 else 0
            insertions = int(insertions[0]) if len(insertions)>0 else 0
            deletions = int(deletions[0]) if len(deletions)>0 else 0
            total_files += files
            total_insertions += insertions
            total_deletions += deletions
            totalprogression.append(total_insertions-total_deletions)
            changed_lines_progression.append(insertions)
    #print("%s: %d files, %d lines" % (hash, total_files,
    #                                      total_insertions - total_deletions))
    #print(totalprogression)
    df = pd.DataFrame({"total": totalprogression, "change": changed_lines_progression}, dates)
    df_daily_change = df["change"].groupby(pd.Grouper(freq='D')).sum()
    plt.plot(dates, totalprogression,
             marker="+",
             label="Total lines",
             c="b",
             )
    #plt.xlabel(r"Time",)
    #TODO: make this fexible, might not always be LaTeX
    plt.ylabel(r"Total \LaTeX\,lines", c="b")
    if diagram_title is not None:
        plt.title(f"{diagram_title}")
    #plt.yscale("log")
    plt.xticks(rotation=25)
    ax = plt.gca()
    ax.spines['left'].set_color('b')
    ax.yaxis.label.set_color('b')
    ax.tick_params(axis='y', colors='b')
    ax_changes = ax.twinx()
    ax_changes.step(df_daily_change.index,
                    df_daily_change,
                    c="r",
                    where="post"
                    )
    #TODO: Same here, could be lines og code
    ax_changes.set_ylabel(r"Added or changed \LaTeX\, lines per day", c="r")
    ax_changes.spines['right'].set_color('r')
    ax_changes.spines['left'].set_color('b')
    ax_changes.yaxis.label.set_color('r')
    ax_changes.tick_params(axis='y', colors='r')
    
    xfmt = md.DateFormatter('%d/%m')
    ax.xaxis.set_major_formatter(xfmt)
    #plt.xlim(datetime.date(2022, 7, 1), datetime.date(2022, 10, 1))
    #ax.vlines([datetime.datetime(2022, 9, 28, 23, 59)], 0, 2800, color='k', linestyle='--')
    #ax.annotate("Deadline", xy= (datetime.date(2022, 9, 20), -100), #xytext=(datetime.date(2022, 9, 15), 2900),
    #)
    plt.tight_layout()
    plt.grid()
    plt.savefig("line_progression_plot.png", dpi=300)
    print("Saved figure")
    plt.show()



if __name__ == '__main__':
    main()
