#!/usr/bin/python3

import argparse
import os
import re


def load_chapters():
    """Load all .tex files in ./chapters"""
    chapters = [path for path in os.listdir("chapters") if ".tex" in path]

    return chapters


def read_file(path) -> list:
    """Reads file from path and returns list of lines"""
    with open(path, "r") as file:
        lines = file.readlines()
    return lines


def check_for_words(list_of_matches: list) -> bool:
    """Redundant as regex can handle minimum word length"""
    duplicate_is_word = False
    for duplicate in list_of_matches:
        if len(duplicate) > 1:
            duplicate_is_word = True
            break
    return duplicate_is_word


def find_double_patterns(lines: list) -> None:
    """Prints the line number of a word repetition"""
    for i, line in enumerate(lines):
        line = line.lower()
        duplicate_matches = re.findall(r"(\b\S{1,}\b)\s+\b\1\b", line)
        if len(duplicate_matches) > 0:
            print(i+1) #correct for line counting beginning at 1
            print(duplicate_matches)


def main() -> None:
    parser = argparse.ArgumentParser(description='Check all files for accidental word duplications')
    parser.add_argument('--files', type=str, nargs='+', help='List of files that should be checked')
    args = parser.parse_args()
    chapters = args.files
    for chap in chapters:
        lines = read_file(chap)
        print(chap)
        find_double_patterns(lines)


if __name__ == "__main__":
    main()
