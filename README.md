# Philipp's LaTeX helpers

## Installation
```shell
source install_latex_scripts.sh
```

## Description
This package provide a couple of tools to check frequent mistakes in LaTeX that are not caught by other tools.
It also provides a tool to track the progress in case the project is version controlled with git.


### Count lines

### Find unreferenced labels
Labels in the document are expected to follow a pattern: \<type\>:\<words connected by a hypen\>, like: eq:schoedinger-stationary
```shell
assert_latex_float_referencing.py --files sections/* [--filter_empty_labels]
```

### Find accidental word duplication
Find accidental duplications in your document and print the line and the duplicated word to the shell.
```shell
double_word_detection.py --files sections/*
```

### Plot you commit history
Plot you current number of lines and your daily activity based on the commit in the local git repository.
Some options for the title and a deadline are available via `--help`
```shell
plot_git_stats.py
```

## Todo
- [ ] Split progress plotting into more functions
- [ ] Create a config, such that title, dealine, etc are not required to be passed as cli arguments per call
