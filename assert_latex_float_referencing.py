#!/usr/bin/python3

import argparse
import re
import itertools
import logging

logging.basicConfig(#filename='example.log', 
#encoding='utf-8',
level=logging.INFO,
)


def find_labels(file, filter_empty_labels=False):
    if filter_empty_labels:
        labels = re.findall(r"\\label{(\w*:{1}[\w|-]+)", file)
    else:
        labels = re.findall(r"\\label{(\w*:{1}[\w|-]*)", file)
    return labels


def find_refs(text, labels, file):
    has_no_missing_label = True
    for label in labels:
        found = re.findall(f"ref\\{{{label}}}", text)
        logging.debug(f"{found=}")
        if not found:
            logging.info(f"Found unreferenced label {label} in {file}")
            has_no_missing_label = False
    return has_no_missing_label


def main() -> None:
    parser = argparse.ArgumentParser(description='Check if all labels (following a built-in pattern) are referenced in your document')
    parser.add_argument('--files', type=str, nargs='+', help='List of files that should be checked')
    parser.add_argument('--filter_empty_labels', help='Filter labels with only a tag', action='store_true')
    args = parser.parse_args()
    files = args.files
    filter_empty_labels =args.filter_empty_labels
    list_of_label_lists = []
    all_labels_referenced = True
    for f in files:
        if ".bak" in f:
            continue
        with open(f"{f}", "r") as file:
            text = file.read()
        labels = find_labels(text, filter_empty_labels=filter_empty_labels)
        list_of_label_lists.append(labels)
        all_labels_referenced_chap = find_refs(text, labels, f)
        if not all_labels_referenced_chap:
            all_labels_referenced = False
    list_of_labels = list(itertools.chain(*list_of_label_lists))
    logging.info(f"Found {len(list_of_labels)} labels")
    logging.info(f"{all_labels_referenced=}")


if __name__ == "__main__":
    main()

